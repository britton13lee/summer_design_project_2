"""
Copyright

Dominic V. Romeo
6/1/2019

TDC2 Implemention.
"""

import serial
import time
import numpy as np
import matplotlib.pyplot as plt

count = 0 

# Setup the serial communication.
ser = serial.Serial( baudrate=115200, 
                     port="COM4",
                     timeout=0)

ser.write("1".encode())
time.sleep(1)

# Data is sent over as raw bytes.
sample = bytearray() 

end = False

# Stay in passive mode till we here something or switch modes.
while not end:
    # Listen for serial traffic.
    while ser.in_waiting: 
        # For each byte we get add it to a byte buffer. 
        sample.extend( ser.read() )
        count = count + 1
        
        if count == 3200:
            end = True

# Convert our byte array into float 32 values. 
latest_sample = np.frombuffer( buffer=sample, dtype=np.float32 )

# Plot the sample we got. 
plt.figure("Data from serial")
plt.subplot(211)
plt.plot(latest_sample)
plt.show()

# Take the FFT to get the frequency spectrum.
n = len(latest_sample)
dft = np.fft.fft(latest_sample)
dft = dft[range(n//2)] # One side of the frequency spectrum. 
plt.subplot(212)
plt.title("FFT")
plt.xlabel("Frequency index k")
plt.ylabel("Magnitude")
normalized_spectrum = abs(dft) / n * 2  # Normalized.
plt.plot(normalized_spectrum) 
plt.show() 

# Determine which frequency is present.
print( f" We got a signal of interest of {soi[0]} hz" )

ser.flushInput()
ser.write("2".encode())
time.sleep(1)
print( ser.readline() )

# Close our serial port once we finish.
ser.close()